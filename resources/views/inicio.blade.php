@extends('layouts.inicio')
<header>
    <!-- Intro settings -->
    <style>
      @media (min-width: 1000px) {
        #intro-example {
          height: 600px;
        }
      }
    </style>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white">
      <div class="container-fluid">
        <button
          class="navbar-toggler"
          type="button"
          data-mdb-toggle="collapse"
          data-mdb-target="#navbarExample01"
          aria-controls="navbarExample01"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <i class="fas fa-bars"></i>
        </button>

      </div>

    </nav>
    <!-- Navbar -->

    <!-- Background image -->
    <div
      id="intro-example"
      class=" text-center bg-image"
      style="background-image: url('https://mdbcdn.b-cdn.net/img/new/slides/041.webp');"

    >
      <div class="mask" style="background-color: rgba(0, 0, 0, 0.7);">
        <div class="d-flex justify-content-center align-items-center h-100">
          <div class="text-white">
            <h1 class="mb-3">UNIVERSIDAD TECNOlÓGICA DE CANCÚN</h1>
            <h2 class="mb-3">BILINGÜE, INTERNACIONAL y SUSTENTABLE</h2>
            <h5 class="mb-4">Sistema de Tickets</h5>
            <a class="btn btn-outline-light btn-lg m-2" href="login"s >Entrar</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Background image -->
  </header>
