<div class="sidebar">
    <nav class="navbar navbar-expand-md navbar-light">

        <ul class="nav">
            @can('dashboard_access')
                <li class="nav-header">INICIO</li>
                    <li class="nav-item">
                        <a href="{{ route("admin.home") }}" class="nav-link">
                            <i class="nav-icon fas fa-fw fa fa-home">

                            </i>
                            {{ trans('global.dashboard') }}
                        </a>
                    </li>
            @endcan
            @can('user_management_access')
                <li class="nav-header">ADMINISTRACIÓN</li>
                    <li class="nav-item nav-dropdown">
                        <a class="nav-link  nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-users nav-icon">

                            </i>
                            {{ trans('cruds.userManagement.title') }}
                        </a>
                        <ul class="nav-dropdown-items">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-unlock-alt nav-icon">

                                        </i>
                                        {{ trans('cruds.permission.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fa-briefcase nav-icon">

                                        </i>
                                        {{ trans('cruds.role.title') }}
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fa-fw fas fas fa-id-card nav-icon">

                                        </i>
                                        {{ trans('cruds.user.title') }}
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
            @endcan
            @can('status_access')
                <li class="nav-header ">CONFIGURACIÓN</li>
                    <li class="nav-item">
                        <a href="{{ route("admin.statuses.index") }}" class="nav-link {{ request()->is('admin/statuses') || request()->is('admin/statuses/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fas fa-exchange-alt nav-icon">

                            </i>
                            {{ trans('cruds.status.title') }}
                        </a>
                    </li>
            @endcan
            @can('priority_access')
                <li class="nav-item">
                    <a href="{{ route("admin.priorities.index") }}" class="nav-link {{ request()->is('admin/priorities') || request()->is('admin/priorities/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fas fa-thermometer-half nav-icon">

                        </i>
                        {{ trans('cruds.priority.title') }}
                    </a>
                </li>
            @endcan
            @can('category_access')
                <li class="nav-item">
                    <a href="{{ route("admin.categories.index") }}" class="nav-link {{ request()->is('admin/categories') || request()->is('admin/categories/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fas fa-stream nav-icon">

                        </i>
                        {{ trans('cruds.category.title') }}
                    </a>
                </li>
            @endcan
            @can('subcategory_access')
                <li class="nav-item">
                    <a href="{{ route("admin.subcategories.index") }}" class="nav-link {{ request()->is('admin/subcategories') || request()->is('admin/subcategories/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fas fa-stream nav-icon">

                        </i>
                        {{ trans('cruds.subcategory.title') }}
                    </a>
                </li>
            @endcan
            @can('edificio_access')
                <li class="nav-item">
                    <a href="{{ route("admin.edificios.index") }}" class="nav-link {{ request()->is('admin/edificio') || request()->is('admin/adificio/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fas fa-stream nav-icon">

                        </i>
                        {{ trans('cruds.edificio.title') }}
                    </a>
                </li>
            @endcan
            @can('espacio_access')     
                <li class="nav-item">
                    <a href="{{ route("admin.espacios.index") }}" class="nav-link {{ request()->is('admin/espacio') || request()->is('admin/espacios/*') ? 'active' : '' }}">
                        <i class="fa-fw fas fas fa-stream nav-icon">

                        </i>
                        {{ trans('cruds.espacio.title') }}
                    </a>
                </li>
            @endcan
            @can('ticket_access')
                <li class="nav-header" >TICKETS</li>
                    <li class="nav-item">
                        <a href="{{ route("admin.tickets.index") }}" class="nav-link {{ request()->is('admin/tickets') || request()->is('admin/tickets/*') ? 'active' : '' }}">
                            <i class="fa-fw fas fa fa-ticket nav-icon">

                            </i>
                            {{ trans('cruds.ticket.title') }}
                        </a>
                    </li>
            @endcan
            @can('comment_access')
                <li class="nav-item">
                    <a href="{{ route("admin.comments.index") }}" class="nav-link {{ request()->is('admin/comments') || request()->is('admin/comments/*') ? 'active' : '' }}">
                        <i class="fa-fw fas far fa-comment-alt nav-icon">

                        </i>
                        {{ trans('cruds.comment.title') }}
                    </a>
                </li>
            @endcan
        </ul>

    </nav>
</div>
