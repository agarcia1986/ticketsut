<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name">{{ trans('cruds.subcategory.fields.name') }}*</label>
    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($subcategory) ? $subcategory->name : '') }}" required>
    @if($errors->has('name'))
        <em class="invalid-feedback">
            {{ $errors->first('name') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.subcategory.fields.name_helper') }}
    </p>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="details">{{ trans('cruds.subcategory.fields.details') }}*</label>
    <input type="text" id="details" name="details" class="form-control" value="{{ old('details', isset($subcategory) ? $subcategory->details : '') }}" required>
    @if($errors->has('details'))
        <em class="invalid-feedback">
            {{ $errors->first('details') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.subcategory.fields.details_helper') }}
    </p>
</div>
