
<div class="form-group{{ $errors->has('title') ? 'has-error' : '' }}">
    <label for="title">{{ trans('cruds.ticket.fields.title') }}*</label>
    <input type="text" id="title" name="title" class="form-control" value="{{ old('title', isset($ticket) ? $ticket->title : '') }}" required>
    @if($errors->has('title'))
        <em class="invalid-feedback">
            {{ $errors->first('title') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.ticket.fields.title_helper') }}
    </p>
</div>
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('solicita') ? 'has-error' : '' }}">
        
        <label for="solicita">{{ trans('cruds.ticket.fields.solicita') }}*</label>
        <!--select class="form-control" name="section" id="section" {{ old('solicita', isset($ticket) ? $ticket->solicita : '') }} required-->
        <select class="form-control" name="solicita" id="solicita" {{ old('solicita', 'kdmsdm') }} required>
        @if (isset($solicita))
        @foreach($solicita as $id => $solicita)
                <option value="{{ $id }}" {{ (isset($solicita)  ? $solicita->id : old('solicita_id')) == $id ? 'selected' : '' }}>{{ $solicita->name }}</option>
            @endforeach  
        @endif
            
        </select>
        @if($errors->has('section'))
            <em class="invalid-feedback">
                {{ $errors->first('section') }}
            </em>
        @endif
        <p class="helper-block">
            {{ trans('cruds.ticket.fields.section_helper') }}
        </p>
    </div>
    <div class="form-group col-md-6 {{ $errors->has('section') ? 'has-error' : '' }}">
        <label for="section">{{ trans('cruds.ticket.fields.section') }}*</label>
        <select class="form-control" name="section" id="section" {{ old('section', 'kdmsdm') }} required>
            @if (isset($espacio))
            @foreach($espacio as $id => $espacio)
            <option value="{{ $id }}" {{ (isset($espacio)  ? $espacio->id : old('espacio_id')) == $id ? 'selected' : '' }}>{{ $espacio->nomenclatura }}</option>
        @endforeach
            @endif
        
        </select>
        @if($errors->has('section'))
            <em class="invalid-feedback">
                {{ $errors->first('section') }}
            </em>
        @endif
        <p class="helper-block">
            {{ trans('cruds.ticket.fields.section_helper') }}
        </p>
    </div>
</div>
<div class="form-group {{ $errors->has('content') ? 'has-error' : '' }}">
    <label for="content">{{ trans('cruds.ticket.fields.content') }}</label>
    <textarea id="content" name="content" class="form-control ">{{ old('content', isset($ticket) ? $ticket->content : '') }}</textarea>
    @if($errors->has('content'))
        <em class="invalid-feedback">
            {{ $errors->first('content') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.ticket.fields.content_helper') }}
    </p>
</div>
<div class="form-group {{ $errors->has('attachments') ? 'has-error' : '' }}">
    <label for="attachments">{{ trans('cruds.ticket.fields.attachments') }}</label>
    <div class="needsclick dropzone" id="attachments-dropzone">

    </div>
    @if($errors->has('attachments'))
        <em class="invalid-feedback">
            {{ $errors->first('attachments') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.ticket.fields.attachments_helper') }}
    </p>
</div>
<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('status_id') ? 'has-error' : '' }}">
        <label for="status">{{ trans('cruds.ticket.fields.status') }}*</label>
        <select name="status_id" id="status" class="form-control select2" required>
            @foreach($statuses as $id => $status)
                <option value="{{ $id }}" {{ (isset($ticket) && $ticket->status ? $ticket->status->id : old('status_id')) == $id || $id == 1 ? 'selected' : '' }}>{{ $status }}</option>
                
            @endforeach
        </select>
        @if($errors->has('status_id'))
            <em class="invalid-feedback">
                {{ $errors->first('status_id') }}
            </em>
        @endif
    </div>
    <div class="form-group col-md-6 {{ $errors->has('priority_id') ? 'has-error' : '' }}">
        <label for="priority">{{ trans('cruds.ticket.fields.priority') }}*</label>
        <select name="priority_id" id="priority" class="form-control select2" required>
            @foreach($priorities as $id => $priority)
                <option value="{{ $id }}" {{ (isset($ticket) && $ticket->priority ? $ticket->priority->id : old('priority_id')) == $id || $id == 1 ? 'selected' : '' }}>{{ $priority }}</option>
            @endforeach
        </select>
        @if($errors->has('priority_id'))
            <em class="invalid-feedback">
                {{ $errors->first('priority_id') }}
            </em>
        @endif
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6 {{ $errors->has('category_id') ? 'has-error' : '' }}">
        <label for="category">{{ trans('cruds.ticket.fields.category') }}*</label>
        <select name="category_id" id="category" class="form-control select2" required>
            @foreach($categories as $id => $category)
                <option value="{{ $id }}" {{ (isset($ticket) && $ticket->category ? $ticket->category->id : old('category_id')) == $id ? 'selected' : '' }}>{{ $category }}</option>
            @endforeach
        </select>
        @if($errors->has('category_id'))
            <em class="invalid-feedback">
                {{ $errors->first('category_id') }}
            </em>
        @endif
    </div>
   
    <div class="form-group col-md-6  {{ $errors->has('subcategory_id') ? 'has-error' : '' }}">
        <label for="subcategory">{{ trans('cruds.ticket.fields.subcategorie') }}*</label>
        <select name="subcategorie_id" id="subcategorie" class="form-control select2" disabled required>
                @if(isset($subcategories))
                    @foreach($subcategories as $id => $subcategory)
                        <option value="{{ $id }}" {{ (isset($ticket) && $ticket->subcategorie_id ? $ticket->subcategorie_id : old('subcategory_id')) == $id ? 'selected' : '' }}>{{ $subcategory }}</option>
                    @endforeach
                @endif
        </select>
        @if($errors->has('subcategorie_id'))
            <em class="invalid-feedback">
                {{ $errors->first('subcategory_id')}}
            </em>
        @endif
    </div>
</div>
@if(auth()->user()->isAdmin() || auth()->user()->isCoordinador() || auth()->user()->isTecnico() )
    <div class="form-group {{ $errors->has('assigned_to_user_id') ? 'has-error' : '' }}">
        <label for="assigned_to_user">{{ trans('cruds.ticket.fields.assigned_to_user') }}</label>
        <select name="assigned_to_user_id" id="assigned_to_user" class="form-control select2">
            @foreach($assigned_to_users as $id => $assigned_to_user)
                <option value="{{ $id }}" {{ (isset($ticket) && $ticket->assigned_to_user ? $ticket->assigned_to_user->id : old('assigned_to_user_id')) == $id ? 'selected' : '' }}>{{ $assigned_to_user }}</option>
            @endforeach
        </select>
        @if($errors->has('assigned_to_user_id'))
            <em class="invalid-feedback">
                {{ $errors->first('assigned_to_user_id') }}
            </em>
        @endif
    </div>
    @endif