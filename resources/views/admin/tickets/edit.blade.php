@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.ticket.title_singular') }}
    </div>

    <div class="card-body">
        <form action="{{ route("admin.tickets.update", [$ticket->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
                @include('admin.tickets.partials.forms')       
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection

@section('scripts')
<script>
  $("#subcategorie").prop('disabled', false);
  /*$( "#category" ).on( "change", function() {
  if($("#category" ).val()){ 
        $("#subcategory").prop('disabled', false);
        let id_categoria= $("#category" ).val();
                $('#subcategory').empty();
                $('#subcategory').append(`<option value="0" disabled selected>Procesando...</option>`);
                $.ajax({
                  type: 'GET',
                  url: 'listsubs/'+id_categoria,
                  success: function (response) {
                    console.log(response);
                    var response = JSON.parse(response);
                       
                    $('#subcategory').empty();
                    $('#subcategory').append(`<option value="0" disabled selected>Seleccione una subcategoria*</option>`);
                    response.forEach(element => {
                        $('#subcategory').append(`<option value="${element['id']}">${element['name']}</option>`);
                      });
                    }
                });
  }else{
    $("#subcategory").prop('disabled', true);
    $('#subcategory').append(`<option value="0" disabled selected>Seleccione una subcategoria*</option>`);
  }
  });*/


$("#section").select2({ 
    language: {
    noResults: function (params) {
      return "No existen resultados";
    }
  },
    placeholder: 'Escribe la letra del edificio',
        ajax: {
          url: 'autocomplete',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            //console.log(data);
            return {
              results:  $.map(data, function (item) {
                //console.log(item);    
                    return {
                        text: item.nomenclatura,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
    });
$("#solicita").select2({ 
    language: {
    noResults: function (params) {
      return "No existen resultados";
    }   
  },
    placeholder: 'Escribe las iniciales del correo',
        ajax: {
          url: 'autocompleteuser',
          dataType: 'json',
          delay: 250,
          processResults: function (data) {
            console.log(data);
            return {
              results:  $.map(data, function (item) {
                //console.log(item);    
                    return {
                        text: item.email,
                        id: item.id
                    }
                })
            };
          },
          cache: true
        }
      });
    var uploadedAttachmentsMap = {}
Dropzone.options.attachmentsDropzone = {
    url: '{{ route('admin.tickets.storeMedia') }}',
    maxFilesize: 2, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="attachments[]" value="' + response.name + '">')
      uploadedAttachmentsMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedAttachmentsMap[file.name]
      }
      $('form').find('input[name="attachments[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($ticket) && $ticket->attachments)
          var files =
            {!! json_encode($ticket->attachments) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="attachments[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@stop
