@extends('layouts.admin')
@section('content')
@can('ticket_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.tickets.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.ticket.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.ticket.title_singular') }} {{ trans('global.list') }}
    </div>
    <div class="card-body">
        <table class=" table table-bordered table-responsive table-striped table-hover ajaxTable datatable datatable-Ticket">
            <thead>
                <tr>
                    <th scope="col">
                        <!--AQUI VAN LAS CASILLAS DE SELECCION-->
                    </th>
                    <th scope="col" >
                        {{ trans('cruds.ticket.fields.id') }}
                    </th>
                    <th scope="col">
                        {{ trans('cruds.ticket.fields.title') }}
                    </th>
                    <th scope="col">
                        {{ trans('cruds.ticket.fields.status') }}
                    </th>
                    <th max-width="3%">
                        {{ trans('cruds.ticket.fields.priority') }}
                    </th>
                    <th scope="col">
                        {{ trans('cruds.ticket.fields.category') }}
                    </th>
                    <th>
                      {{ trans('cruds.ticket.fields.subcategorie') }}
                  </th>
                    <th scope="col">
                      {{ trans('cruds.ticket.fields.building') }}
                  </th> 
                    <th scope="col">
                        {{ trans('cruds.ticket.fields.assigned_to_user') }}
                    </th>
                    <th>
                       Acciones
                    </th>
                </tr>
            </thead>
        </table>


    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
      let filters = `
                <form class="form-inline" id="filtersForm">
                  <div class="form-group mx-sm-3 mb-2">
                    <select class="form-control" name="status">
                      <option value="">Todos los Status</option>
                      @foreach($statuses as $status)
                        <option value="{{ $status->id }}"{{ request('status') == $status->id ? 'selected' : '' }}>{{ $status->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <select class="form-control" name="priority">
                      <option value="">Todas las Prioridades</option>
                      @foreach($priorities as $priority)
                        <option value="{{ $priority->id }}"{{ request('priority') == $priority->id ? 'selected' : '' }}>{{ $priority->name }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group mx-sm-3 mb-2">
                    <select class="form-control" name="category">
                      <option value="">Tipo de Tickets</option>
                      @foreach($categories as $category)
                        <option value="{{ $category->id }}"{{ request('category') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                      @endforeach
                    </select>
                  </div>
                </form>`;
      $('.card-body').on('change', 'select', function() {
        $('#filtersForm').submit();
      })
      let dtButtons = []
      @can('ticket_delete')
        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
        let deleteButton = {
          text: deleteButtonTrans,
          url: "{{ route('admin.tickets.massDestroy') }}",
          className: 'btn-danger',
          action: function (e, dt, node, config) {
            var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
                return entry.id
          });
          if (ids.length === 0) {
            alert('{{ trans('global.datatables.zero_selected') }}')
            return
          }
          if (confirm('{{ trans('global.areYouSure') }}')) {
            $.ajax({
              headers: {'x-csrf-token': _token},
              method: 'POST',
              url: config.url,
              data: { ids: ids, _method: 'DELETE' }})
              .done(function () { location.reload() })
          }
        }
      }
  dtButtons.push(deleteButton)
  @endcan
  let searchParams = new URLSearchParams(window.location.search)
  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: {
      url: "{{ route('admin.tickets.index') }}",
      data: {
        'status': searchParams.get('status'),
        'priority': searchParams.get('priority'),
        'category': searchParams.get('category')
      }
    },
    columns: [
      { data: 'placeholder', name: 'placeholder' },
      { data: 'id', name: 'id' },
      {
        data: 'title',
        name: 'title',
          render: function ( data, type, row) {
      //
              return '<a href="'+row.view_link+'">'+data+' ('+row.comments_count+')</a>';         
          }
      },
      {
        data: 'status_name',
        name: 'status.name',
        render: function ( data, type, row) {
            return '<span style="color:'+row.status_color+'">'+data+'</span>';
        }
      },
      {
        data: 'priority_name',
        name: 'priority.name',
        render: function ( data, type, row) {
            //return '<span style="color:'+row.priority_color+'">'+data+'</span>';
            return '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="'+row.priority_color+'" class="bi bi-stoplights-fill" viewBox="0 0 16 16">  <path fill-rule="evenodd" d="M6 0a2 2 0 0 0-2 2H2c.167.5.8 1.6 2 2v2H2c.167.5.8 1.6 2 2v2H2c.167.5.8 1.6 2 2v1a2 2 0 0 0 2 2h4a2 2 0 0 0 2-2v-1c1.2-.4 1.833-1.5 2-2h-2V8c1.2-.4 1.833-1.5 2-2h-2V4c1.2-.4 1.833-1.5 2-2h-2a2 2 0 0 0-2-2zm3.5 3.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m0 4a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0M8 13a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3"/></svg>';
        }
      },
      {
        data: 'category_name',
        name: 'category.name',
        render: function ( data, type, row) {
            return '<span style="color:'+row.category_color+'">'+data+'</span>';
        }
      },
{
  data: 'subcategorie',
  name: 'subcategorie.name',
  render: function ( data, type, row) {
      return '<p class="text-xs text-gray-400">'+data+'</p>';
  }
},
{
  data: 'espacio',
  name: 'espacio.nomenclatura',
  render: function ( data, type, row) {
      return '<span>'+data+'</span>';
  }
},
// { data: 'author_name', name: 'author_name' },
// { data: 'author_email', name: 'author_email' },
{ data: 'assigned_to_user_name', name: 'assigned_to_user.name' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 50,
  };
$(".datatable-Ticket").one("preInit.dt", function () {
  $(".dataTables_filter").after(filters);
});
  $('.datatable-Ticket').DataTable(dtOverrideGlobals,{responsive : true});
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      
      $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection
