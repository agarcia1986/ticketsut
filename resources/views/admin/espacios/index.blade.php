@extends('layouts.admin')
@section('content')
@can('espacio_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.espacios.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.espacio.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.espacio.title_singular') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Espacio">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.espacio.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.espacio.fields.nomenclatura') }}
                        </th>
                        <th>
                            {{ trans('cruds.espacio.fields.detalle') }}
                        </th>
                        <th>
                            {{ trans('cruds.espacio.fields.capacidad') }}
                        </th>
                       
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($espacios as $key => $espacio)
                        <tr data-entry-id="{{ $espacio->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $espacio->id ?? '' }}
                            </td>
                            <td>
                                {{ $espacio->nomenclatura ?? '' }}
                            </td>
                            <td>
                                {{ $espacio->detalle ?? '' }}
                            </td>
                            <td>
                                {{ $espacio->capacidad ?? '' }}
                            </td>
                            <!--td style="background-color:{{ $espacio->color ?? '#FFFFFF' }}"></td-->
                            <td>
                                @can('espacio_show')
                                    <!--a class="btn btn-xs btn-primary" href="{{ route('admin.espacios.show', $espacio->id) }}">
                                        {{ trans('global.view') }}
                                    </a-->
                                @endcan

                                @can('espacio_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.espacios.edit', $espacio->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('espacio_delete')
                                    <form action="{{ route('admin.espacios.destroy', $espacio->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
$(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
    @can('espacio_delete')
        let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.espacios.massDestroy') }}",
            className: 'btn-danger',
            action: function (e, dt, node, config) {
                var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
                    return $(entry).data('entry-id')
            });
                if (ids.length === 0) {
                    alert('{{ trans('global.datatables.zero_selected') }}')
                    return
                }

                if (confirm('{{ trans('global.areYouSure') }}')) {
                    $.ajax({
                        headers: {'x-csrf-token': _token},
                        method: 'POST',
                        url: config.url,
                        data: { ids: ids, _method: 'DELETE' }})
                        .done(function () { location.reload() })
                }
            }
        }
        dtButtons.push(deleteButton)
    @endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Espacio:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})
</script>
@endsection