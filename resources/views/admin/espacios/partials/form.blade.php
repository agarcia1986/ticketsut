            <div class="form-group {{ $errors->has('nomenclatura') ? 'has-error' : '' }}">
                <label for="nomenclatura">{{ trans('cruds.espacio.fields.nomenclatura') }}*</label>
                <input type="text" id="nomenclatura" name="nomenclatura" class="form-control" value="{{ old('nomenclatura', isset($espacio) ? $espacio->nomenclatura : '') }}" required>
                @if($errors->has('nomenclatura'))
                    <em class="invalid-feedback">
                        {{ $errors->first('nomenclatura') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.espacio.fields.nomenclatura_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('detalle') ? 'has-error' : '' }}">
                <label for="detalle">{{ trans('cruds.espacio.fields.detalle') }}*</label>
                <input type="text" id="detalle" name="detalle" class="form-control" value="{{ old('detalle', isset($espacio) ? $espacio->detalle : '') }}" required>
                @if($errors->has('detalle'))
                    <em class="invalid-feedback">
                        {{ $errors->first('detalle') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.espacio.fields.detalle_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('edificio_id') ? 'has-error' : '' }}">
                <label for="edificio">{{ trans('cruds.espacio.fields.edificio') }}*</label>
                <select name="edificio" id="edificio" class="form-control"  required>
                    @foreach($edificios as $id => $edificio)
                        <option value="{{ $id }}" {{ (isset($espacio) && $espacio->edificio_id ? $espacio->edificio_id : old('edificio_id')) == $id ?  'selected' : '' }}>{{ $edificio }}</option>
                    @endforeach
                </select>
                @if($errors->has('edificio_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('edificio_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.espacio.fields.edificio_helper') }}
                </p>
            </div>

            <div class="form-group {{ $errors->has('tipoEspacio_id') ? 'has-error' : '' }}">
                <label for="tipoespacio">{{ trans('cruds.espacio.fields.tipoespacio') }}*</label>
                <select name="tipoEspacio_id" id="tipoEspacio_id" class="form-control"  required>
                    @foreach($tipoEspacios as $id => $tipoespacio)
                        <option value="{{ $id }}" {{ (isset($espacio) && $espacio->tipoEspacio_id ? $espacio->tipoEspacio_id : old('tipoEspacio_id')) == $id ?  'selected' : '' }}>{{ $tipoespacio }}</option>
                    @endforeach
                </select>
                @if($errors->has('tipoEspacio_id'))
                    <em class="invalid-feedback">
                        {{ $errors->first('tipoEspacio_id') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.espacio.fields.tipoespacio_helper') }}
                </p>
            </div>
            <div class="form-group {{ $errors->has('capacidad') ? 'has-error' : '' }}">
                <label for="capacidad">{{ trans('cruds.espacio.fields.capacidad') }}*</label>
                <input type="number" id="capacidad" name="capacidad" class="form-control" value="{{ old('capacidad', isset($espacio) ? $espacio->capacidad : '') }}" required>
                @if($errors->has('capacidad'))
                    <em class="invalid-feedback">
                        {{ $errors->first('capacidad') }}
                    </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.espacio.fields.capacidad_helper') }}
                </p>
            </div>
           
            