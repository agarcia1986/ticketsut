@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.espacio.title_singular') }}
    </div>
    <div class="card-body">
        <form action="{{ route("admin.espacios.store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            @include('admin.espacios.partials.form')
            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')

@endsection