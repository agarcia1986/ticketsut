<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    <label for="name">{{ trans('cruds.category.fields.name') }}*</label>
    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($category) ? $category->name : '') }}" required>
    @if($errors->has('name'))
        <em class="invalid-feedback">
            {{ $errors->first('name') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.category.fields.name_helper') }}
    </p>
</div>
<div class="form-group {{ $errors->has('color') ? 'has-error' : '' }}">
    <label for="color">{{ trans('cruds.category.fields.color') }}</label>
    <input type="text" id="color" name="color" class="form-control colorpicker" value="{{ old('color', isset($category) ? $category->color : '') }}">
    @if($errors->has('color'))
        <em class="invalid-feedback">
            {{ $errors->first('color') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.category.fields.color_helper') }}
    </p>
</div>
<div class="form-group {{ $errors->has('subcategorias') ? 'has-error' : '' }}">
    <label for="permissions">{{ trans('cruds.category.fields.subcategories') }}*
        <span class="btn btn-info btn-xs select-all">{{ trans('global.select_all') }}</span>
        <span class="btn btn-info btn-xs deselect-all">{{ trans('global.deselect_all') }}</span></label>
    <select name="categories[]" id="categories" class="form-control select2" multiple required>
        @foreach($subcategorias as $id => $subcategorie)
            <option value="{{ $id }}" {{ (in_array($id, old('categories', [])) || isset($category) && $category->subcategorias->contains($id)) ? 'selected' : '' }}>{{ $subcategorie }}</option>
        @endforeach
    </select>
    @if($errors->has('permissions'))
        <em class="invalid-feedback">
            {{ $errors->first('permissions') }}
        </em>
    @endif
    <p class="helper-block">
        {{ trans('cruds.role.fields.permissions_helper') }}
    </p>
</div>