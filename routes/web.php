<?php

use App\Http\Controllers\Admin\EspaciosController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\InicioController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
// use Illuminate\Support\Facades\Hash;
// use Illuminate\Support\Facades\Broadcast;
use App\Http\Controllers\MailController;
//use App\Http\Controllers\InicioController;


//BUSQUEDA DE ESPACIOS AJAX
Route::get('select', [EspaciosController::class, 'index'])->name('select');
Route::get('admin/tickets/autocomplete', [EspaciosController::class, 'autocompleteEspacio'])->name('autocomplete');
Route::get('admin/tickets/{ticket}/autocomplete', [EspaciosController::class, 'autocompleteEspacio'])->name('autocomplete');
//BUSQUEDA USUARIOS AJAX
Route::get('admin/tickets/autocompleteuser', [UsersController::class, 'autocompleteuser'])->name('autocompleteuser');
Route::get('admin/tickets/{ticket}/autocompleteuser', [UsersController::class, 'autocompleteuser'])->name('autocompleteuser');
//BUSQUEDA SUBCATEGORIAS AJAX
//Route::get('admin/subcategories/listsubs', [SubcategoriesController::class, 'listsubs'])->name('listsubs');
Route::get('admin/tickets/listsubs/{category}', [CategoriesController::class, 'listsubs'])->name('listsubs');


Route::get('/', 'InicioController@index');
//Route::get('/','App\Http\Controllers\InicioController@index');
Route::get('/home', function () {
    $route = Gate::denies('dashboard_access') ? 'admin.tickets.index' : 'admin.home';
    if (session('status')) {
        return redirect()->route($route)->with('status', session('status'));
    }
    return redirect()->route($route);
});

Auth::routes(['register' => false]);
Route::get('sendmail', [MailController::class, 'index'])->name('sendmail');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Statuses
    Route::delete('statuses/destroy', 'StatusesController@massDestroy')->name('statuses.massDestroy');
    Route::resource('statuses', 'StatusesController');

    // Priorities
    Route::delete('priorities/destroy', 'PrioritiesController@massDestroy')->name('priorities.massDestroy');
    Route::resource('priorities', 'PrioritiesController');

    // Categories
    Route::delete('categories/destroy', 'CategoriesController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'CategoriesController');
    // Subcategories
    Route::delete('subcategories/destroy', 'SubcategoriesController@massDestroy')->name('subcategories.massDestroy');
    Route::resource('subcategories', 'SubcategoriesController');

    //Edificios
    Route::delete('edificios/destroy', 'EdificiosController@massDestroy')->name('edificios.massDestroy');
    Route::resource('edificios', 'EdificiosController');

    //Espacios
    Route::delete('espacios/destroy', 'EspaciosController@massDestroy')->name('espacios.massDestroy');
    Route::resource('espacios', 'EspaciosController');

    // Tickets
    Route::delete('tickets/destroy', 'TicketsController@massDestroy')->name('tickets.massDestroy');
    Route::post('tickets/media', 'TicketsController@storeMedia')->name('tickets.storeMedia');
    Route::post('tickets/comment/{ticket}', 'TicketsController@storeComment')->name('tickets.storeComment');
    Route::resource('tickets', 'TicketsController');

    // Comments
    Route::delete('comments/destroy', 'CommentsController@massDestroy')->name('comments.massDestroy');
    Route::resource('comments', 'CommentsController');    
});
