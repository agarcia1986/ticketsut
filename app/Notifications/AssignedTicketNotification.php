<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AssignedTicketNotification extends Notification
{
    use Queueable;

   
    public function __construct($ticket)
    {
        $this->ticket = $ticket;
    }

    
    public function via($notifiable)
    {
        return ['mail'];
    }

   
    public function toMail($notifiable)
    {
        //return "llega";
        return (new MailMessage)
        ->subject('Se le ha asignado un nuevo Ticket')
        ->greeting('Hola,')
        ->line('Se te ha asignado un nuevo ticket: '.$this->ticket->title)
        ->action('Ver ticket', route('admin.tickets.show', $this->ticket->id))
        ->line('Gracias')
        ->line(config('app.name'))
        ->salutation(' ');
    }
}
