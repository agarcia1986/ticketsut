<?php

namespace App\Observers;

use App\Notifications\DataChangeEmailNotification;
use App\Notifications\AssignedTicketNotification;
use App\Models\Ticket;
use App\Models\User;
use App\Models\Espacio;
use Illuminate\Support\Facades\Notification;
use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Mailer;

class TicketActionObserver
{
    public function created(Ticket $model)
    {
        //return $model;
        //$data  = ['action' => '¡Se ha creado un nuevo ticket!', 'model_name' => 'Ticket', 'ticket' => $model];
        $usuarioRequest= User::where('id', $model->user_request_id)->first();
        $usuarioAsignado=User::where('id', $model->assigned_to_user_id)->first();
        $nombreEspacio=Espacio::where('id', $model->espacio_id)->first();
        $dsn='smtp://mantenimiento@utcancun.edu.mx:Num11812@smtp.office365.com:587';
        $transport= Transport::fromDsn($dsn);  
        
        $notificacionAssignado =(new Email())
            ->from('mantenimiento@utcancun.edu.mx')
            ->to($usuarioAsignado->email)
            //->to('aureliogarcia5@gmail.com')
            ->subject('Tienes un nuevo Ticket '.$model->title.' .')
            ->html('<h2>'.$usuarioAsignado->email.'Se te ha asignado un nuevo ticket : '.$model->title.'<H2>
                    <h2>Descripción :'.$model->content.'</h2>
                    <H2>Área que solicita el Servicio : '.$nombreEspacio->nomenclatura.'<H2>
                    <h2>Fecha : '.date("d/m/Y",strtotime($model->created_at)).'</h2>');
        $notificacionUser =(new Email())
            ->from('mantenimiento@utcancun.edu.mx')
            ->to($usuarioRequest->email)
            //->to('aureliogarcia5@gmail.com')
            ->subject('Apertura de ticket solicitado '.$model->title.'.')
            ->html('<H2>Estimado '.$usuarioRequest->name.' : le envió información del ticket solicitado para su seguimiento.</H2>  
                    <H2>Área que solicita el Servicio : '.$nombreEspacio->nomenclatura.'<H2>
                    <H2>Categoría : '.$model->category->name.'</H2>
                    <H2>Numero de ticket : '.$model->id.'</H2>
                    <H2>Detalle de la solicitud : '.$model->content.'</H2>
                    <H2>Horario disponible para realizar actividad : Por definir</H2>
                    <h2>Ubicación : '.$nombreEspacio->nomenclatura.'</h2>
                    <h2>Fecha : '.date("d/m/Y",strtotime($model->created_at)).'</h2>
                    <H2>FAVOR DE NO RESPONDER ESTE E-MAIL</H2>
                    <H4>Departamento de Mantenimiento y Servicios Generales<H4>
                    <h4>Tel. (998) 8811900 Ext 1071</h4>
                    <h4>mantenimiento@utcancun.edu.mx</h4>');
        $notificando = new Mailer($transport);  
        $notificando->send($notificacionAssignado);

        $notificandoUser = new Mailer($transport);  
        $notificandoUser->send($notificacionUser);
        

        }   

    public function updated(Ticket $model)
    {
        if($model->isDirty('assigned_to_user_id'))
        {
            $user = $model->assigned_to_user_id;
            $nombreEspacio=Espacio::where('id', $model->espacio_id)->first();
            $usuarioReasignado= User::where('id', $user)->first(); 
            $usuarioRequest= User::where('id', $model->user_request_id)->first();    
                    $dsn='smtp://mantenimiento@utcancun.edu.mx:Num11812@smtp.office365.com:587';
                    $transport= Transport::fromDsn($dsn);                     
                    $notificacion =(new Email())
                        ->from('mantenimiento@utcancun.edu.mx')
                        ->to($usuarioReasignado->email)
                        //->to('aureliogarcia5@gmail.com')
                        ->subject('Ticket '.$model->title .'reasignado')
                        ->html('<h2>Se te ha reasignado un ticket : '.$model->title.'<H2>
                            <h2>Descripción: '.$model->content.'</h2>
                            <H2>Ubucación del Servicio : '.$nombreEspacio->nomenclatura.'<H2>
                            <H2>Solicitó: '.$usuarioRequest->name.'</H2>
                            <h2>Fecha : '.date("d/m/Y",strtotime($model->updated_at)).'</h2>');
                        //->text($data);
                        
                    $notificando = new Mailer($transport);    
                    $notificando->send($notificacion);
        }
        if($model->isDirty('status_id'))
        {
            $user = $model->user_request_id;
            if($user)
            {
                if($model->status->id == 2)
                { 
                    $usuariorequest= User::where('id', $user)->first();       
                    $dsn='smtp://mantenimiento@utcancun.edu.mx:Num11812@smtp.office365.com:587';
                    $transport= Transport::fromDsn($dsn);  
                    $notificacion =(new Email())
                        ->from('mantenimiento@utcancun.edu.mx')
                        ->to($usuariorequest->email)
                        //->to('aureliogarcia5@gmail.com')
                        ->subject('Ticket '.$model->id.' cerrado')
                        ->html('<h2>Estimado(a) : ' .$usuariorequest->name.'</h2> 
                                <h2>Se ha cerrado el ticket :'.$model->title.'<h2>
                                <h2>Descripción :' .$model->content.'</h2>
                                <h2>Ubicación :'.$model->espacio->nomenclatura.'</h2>
                                <h2>Por favor ayudanos a evaluar nuestro servicio <a href="http://www.utcancun.edu.mx/13.1.23.rie/index.php/survey/index/sid/778689/newtest/Y/lang/es-MX/ticketID/'.$model->id.'">AQUI</a> </h2>  
                                <h2>Fecha: '.date("d/m/Y",strtotime($model->created_at)).'</h2> 
                                <H4>Departamento de Mantenimiento y Servicios Generales<H4>
                                <p>Tel. (998) 8811900 Ext 1071</p>
                                <p>mantenimiento@utcancun.edu.mx</p>');
                        //->text($data);
                        
                    $notificando = new Mailer($transport);    
                    $notificando->send($notificacion);
                }
            }        
        }
    }
}
