<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tipoEspacio extends Model
{
    use HasFactory;
    protected $fillable = [
        "nombre"
    ];
    public function espacios(){
        return $this->hasMany('App\Models\Espacio');
    }

}
