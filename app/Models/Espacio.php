<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Espacio extends Model
{
    use HasFactory;
    protected $fillable = [
        'nomenclatura',
        'edificio_id',
        'tipoEspacio_id',
        'detalle',
        'capacidad'
    ];
    public function edificios(){
        return $this->belongsTo('App\Models\Edificio');    
    }
    public function tipoEspacios(){
        return $this->belongsTo('App\Models\tipoEspacio');
    }
    public function Ticket(){
        return $this->hasMany('App\Models\Ticket');
    }
}
