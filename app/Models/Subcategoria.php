<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Subcategoria extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'details'
    ];

    public function Categories()
    {
        return $this->belongsToMany(Category::class);
    }
    public function tickets()
    {
        return $this->hasMany(Ticket::class, 'subcategorie_id', 'id');
    }
}
