<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Edificio;
use Illuminate\Http\Request;
use App\Models\Espacio;
use App\Models\tipoEspacio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
class EspaciosController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('espacio_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $espacios = Espacio::all();
        return view('admin.espacios.index', compact('espacios'));
    }

    public function create()
    {
        abort_if(Gate::denies('espacio_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $edificios = Edificio::all()->pluck('nombre','id');
        $tipoEspacios= tipoEspacio::all()->pluck('nombre','id');
        return view('admin.espacios.create',compact('edificios','tipoEspacios'));
    }
    public function store(Request $request)
    {
        $espacio = Espacio::create([
            'nomenclatura'  => $request->nomenclatura,
            'edificio_id'   =>$request->edificio,
            'tipoEspacio_id' =>$request->tipoespacio,
            'detalle'       =>$request->detalle,
            'capacidad'     =>$request->capacidad
        ]);
        return redirect()->route('admin.espacios.index');
    }    
    public function show($id)
    {
        //
    }

    public function edit(Espacio $espacio)
    {
        //return $espacio;
        abort_if(Gate::denies('espacio_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $edificios = Edificio::all()->pluck('nombre','id');
        $tipoEspacios= tipoEspacio::all()->pluck('nombre','id');
        return view('admin.espacios.edit',compact('espacio','edificios','tipoEspacios'));
    }
    public function update(Request $request ,Espacio $espacio)
    {
        //return $request;
            $espacio->update($request->all());
        return redirect()->route('admin.espacios.index');
    }

    
    public function destroy(Espacio $espacio)
    {   
        abort_if(Gate::denies('espacio_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        //return "prueba";
        $espacio->delete();
        return back();
    }
    public function autocompleteEspacio(Request $request)
    {
        $data = [];
        if($request->has('q')){
            $search = $request->q;
            $data = Espacio::select("id", DB::raw("CONCAT(espacios.nomenclatura,' ',espacios.detalle) AS nomenclatura"))
            ->where('nomenclatura','LIKE',"%$search%")
            ->orWhere('detalle','LIKE',"%$search%")
            ->get();
        }
        return response()->json($data);
    }
}
