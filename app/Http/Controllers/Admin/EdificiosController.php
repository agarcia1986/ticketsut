<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Edificio;

class EdificiosController extends Controller
{
    
    public function index()
    {
        abort_if(Gate::denies('edificio_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $edificios = Edificio::all();

        return view('admin.edificios.index', compact('edificios'));
    }

   
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

    
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
