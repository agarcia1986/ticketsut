<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subcategoria;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;

class SubcategoriesController extends Controller
{
    
    public function index()
    {
        abort_if(Gate::denies('subcategory_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subcategories = Subcategoria::all();
        return view('admin.subcategories.index', compact('subcategories'));
    }

   
    public function create()
    {
        abort_if(Gate::denies('subcategory_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.subcategories.create');
    }

   
    public function store(Request $request)
    {
        $subcategory = Subcategoria::create($request->all());
        return redirect()->route('admin.subcategories.index');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit(Subcategoria $subcategory)
    {
        //return $subcategory;
        abort_if(Gate::denies('subcategory_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.subcategories.edit',compact('subcategory'));
    }

    public function update(Request $request, Subcategoria $subcategory)
    {
        $subcategory->update($request->all());
        return redirect()->route('admin.subcategories.index');
    }

   
    public function destroy(Subcategoria $subcategory)
    {
        abort_if(Gate::denies('subcategory_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $subcategory->delete();

        return back();
    }
    public function massDestroy(Request $request)
    {
        Subcategoria::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
