<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEspaciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('espacios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomenclatura',8);
            $table->unsignedBigInteger('edificio_id');
            $table->unsignedBigInteger('tipoEspacio_id');
            $table->string('detalle')->nullable ();
            $table->integer('capacidad')->nullable();
            $table->foreign('edificio_id')->references('id')->on('edificios')->onDelete('cascade');
            $table->foreign('tipoEspacio_id')->references('id')->on('tipo_espacios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('espacios');
    }
}
