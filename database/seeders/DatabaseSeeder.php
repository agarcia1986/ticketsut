<?php
namespace Database\Seeders;
//use Database\Seeders\EdificiosSeeder;
//use Database\Seeders\EspaciosSeeder;
//use Database\Seeders\TipoEspaciosSeeder;
//use Database\Sedders\SubcategoriaSeeder;
//use Database\Seeders\CategoriaSubcategoriaSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            CategoriesTableSeeder::class,
            StatusesTableSeeder::class,
            PrioritiesTableSeeder::class,
            TipoEspacioSeeder::class,
            EdificiosSeeder::class,
            EspaciosSeeder::class,
            SubcategoriaSeeder::class,
            CategoriaSubcategoriaTableSeeder::class  
        ]);
    }
}
