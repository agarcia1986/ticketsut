<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use \App\Models\Category;
use Faker\Factory as Faker;
class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        
        $categories = [
            "MANTENIMIENTO","SERVICIOS GENERALES","VEHÍCULOS"];

        foreach($categories as $category)
        {
            Category::create([
                'name'  => $category,
                'color' => $faker->hexcolor
            ]);
        }
    }
}
