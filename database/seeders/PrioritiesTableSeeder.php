<?php
namespace Database\Seeders;
use App\Models\Priority;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class PrioritiesTableSeeder extends Seeder
{
    
    public function run()
    {
        $faker = Faker::create();
        $priorities = [
            'Baja', 'Media', 'Alta'
        ];

        foreach($priorities as $priority)
        {
            Priority::create([
                'name'  => $priority,
                'color' => $faker->hexcolor
            ]);
        }
    }
}
