<?php
namespace Database\Seeders;

use App\Models\Edificio;
use Illuminate\Database\Seeder;

class EdificiosSeeder extends Seeder
{
   
    public function run()
    {
        $edificios =[
            [
                "id"=>1,
                "nombre"=>"A"
            ],
            [
                "id"=>2,
                "nombre"=>"B"
            ],
            [
                "id"=>3,
                "nombre"=>"C"
            ],
            [
                "id"=>4,
                "nombre"=>"D"
            ],
            [
                "id"=>5,
                "nombre"=>"E"
            ],
            [
                "id"=>6,
                "nombre"=>"F"
            ],
            [
                "id"=>7,
                "nombre"=>"G"
            ],
            [
                "id"=>8,
                "nombre"=>"H"
            ],
            [
                "id"=>9,
                "nombre"=>"J"
            ],
            [
                "id"=>10,
                "nombre"=>"K"
            ],
            [
                "id"=>11,
                "nombre"=>"M"
            ],
            [
                "id"=>12,
                "nombre"=>"EXTERIOR"
            ]
            ];
            Edificio::insert($edificios);
    }
}
