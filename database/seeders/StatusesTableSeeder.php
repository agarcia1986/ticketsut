<?php
namespace Database\Seeders;
use App\Models\Status;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $statuses = [
            'Abierto', 'Cerrado','En espera'
        ];

        foreach($statuses as $status)
        {
            Status::create([
                'name'  => $status,
                'color' => $faker->hexcolor
            ]);
        }
    }
}
