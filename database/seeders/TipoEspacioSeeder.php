<?php
namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\tipoEspacio;

class TipoEspacioSeeder extends Seeder
{
    
    public function run()
    {
        $tipo_espacio=[
            ["id"=>1,
            "nombre"=>"Salon"
            ],
            ["id"=>2,
            "nombre"=>"Oficina"
            ],
            ["id"=>3,
            "nombre"=>"Cubículo"
            ],
            ["id"=>4,
            "nombre"=>"Audiovisual"
            ],
            ["id"=>5,
            "nombre"=>"Laboratorio"
            ],
            ["id"=>6,
            "nombre"=>"Sala de juntas"
            ],
            ["id"=>7,
            "nombre"=>"Comedor"
            ],
            ["id"=>8,
            "nombre"=>"Archivos"
            ],
            ["id"=>9,
            "nombre"=>"Taller"
            ],
            ["id"=>10,
            "nombre"=>"Bodega"
            ],
            ["id"=>11,
            "nombre"=>"Baño"
            ],
            ["id"=>12,
            "nombre"=>"Otro"
            ],
        ];
        tipoEspacio::insert($tipo_espacio);
    }
}
