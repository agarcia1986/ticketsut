<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Subcategoria;

class SubcategoriaSeeder extends Seeder
{
    public function run()
    {
        $subcategorias = [
            [
                'id'    => 1,
                'name' => 'Aires Acondicionados',
                'details' =>'Aires Acondicionados',
            ],
            [
                'id'    => 2,
                'name' => 'Instalaciones Hidráulicas y Sanitarias',
                'details' =>'Instalaciones Hidráulicas y Sanitarias',
            ],
            [
                'id'    => 3,
                'name' => 'Instalaciones eléctricas',
                'details' =>'Instalaciones eléctricas',
            ],
            [
                'id'    => 4,
                'name' => 'Cancelería',
                'details' =>'Cancelería',
            ],
            [
                'id'    => 5,
                'name' => 'Obra Civil',
                'details' =>'Obra Civil',
            ],
            [
                'id'    => 6,
                'name' => 'Mobiliario y Equipo',
                'details' =>'Mobiliario y Equipo',
            ],
            [
                'id'    => 7,
                'name' => 'Supervisión-Revisión',
                'details' =>'Supervisión-Revisión',
            ],
            [
                'id'    => 8,
                'name' => 'Eventos',
                'details' =>'Eventos',
            ],
            [
                'id'    => 9,
                'name' => 'Instalaciones',
                'details' =>'Instalaciones',
            ],
            [
                'id'    => 10,
                'name' => 'Apoyo',
                'details' =>'Apoyo',
            ],
            [
                'id'    => 11,
                'name' => 'Llenado de combustible',
                'details' =>'Llenado de combustible',
            ],
            [
                'id'    => 12,
                'name' => 'Mantenimiento de la unidad',
                'details' =>'Mantenimiento de la unidad',
            ],
            [
                'id'    => 13,
                'name' => 'Traslados',
                'details' =>'Traslados',
            ],
            [
                'id'    => 14,
                'name' => 'Visitas',
                'details' =>'Visitas',
            ],
            [
                'id'    => 15,
                'name' => 'Comisión',
                'details' =>'Comisión',
            ]
        ];

        Subcategoria::insert($subcategorias);
    }
}
