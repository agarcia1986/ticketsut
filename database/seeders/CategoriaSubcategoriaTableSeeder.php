<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriaSubcategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Category::findOrfail(1)->Subcategorias()->sync(range(1,7));
        Category::findOrfail(2)->Subcategorias()->sync(range(8,10));
        Category::findOrfail(3)->Subcategorias()->sync(range(11,15));
       
    }
}
