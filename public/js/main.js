$(document).ready(function () {
  window._token = $('meta[name="csrf-token"]').attr('content')

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
        allEditors[i],
        {
            removePlugins: ['ImageUpload']
        }
    );
  }

  moment.updateLocale('en', {
    week: {dow: 1} // Monday is the first day of the week
  })

  $('.date').datetimepicker({
    format: 'YYYY-MM-DD',
    locale: 'en'
  })

  $('.datetime').datetimepicker({
    format: 'YYYY-MM-DD HH:mm:ss',
    locale: 'en',
    sideBySide: true
  })

  $('.timepicker').datetimepicker({
    format: 'HH:mm:ss'
  })
$('#select2').select2({
  tags: true,
  width: '100%', // Establece el ancho del contenedor del cuadro de lista
  closeOnSelect: false // Evita que el cuadro de opciones se cierre automáticamente al seleccionar una opción
});

/**CODIGO PARA LOS SELECTS MULTIPLES CON BOTONES SELECT ALL Y DESELECT ALL **/
$('#Select2').select2('open');

            // Botón para seleccionar todos
            $('.select-all').click(function() {
              
                $('.select2').find('option').prop('selected', true);
                $('.select2').trigger('change'); // Notifica a Select2 sobre el cambio
                // Abre el cuadro de opciones de Select2 al cargar la página
            $('#Select2').select2('open');
            });

            // Botón para deseleccionar todos
            $('.deselect-all').click(function() {
              //console.log('funca');
                $('.select2').find('option').prop('selected', false);
                $('.select2').trigger('change'); // Notifica a Select2 sobre el cambio
                // Abre el cuadro de opciones de Select2 al cargar la página
            $('#Select2').select2('open');
            });



  $('.treeview').each(function () {
    var shouldExpand = false
    $(this).find('li').each(function () {
      if ($(this).hasClass('active')) {
        shouldExpand = true
      }
    })
    if (shouldExpand) {
      $(this).addClass('active')
    }
  })

 
})
